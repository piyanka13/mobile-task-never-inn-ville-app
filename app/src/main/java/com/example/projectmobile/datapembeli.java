package com.example.projectmobile;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmobile.Certificate.SelfSigningClient;
import com.example.projectmobile.Interface.PropertiInterface;
import com.example.projectmobile.Model.Pembeli;
import com.example.projectmobile.Model.Pembelian;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class datapembeli extends AppCompatActivity {
    //variable for datepicker
    private int mYear;
    private int mMonth;
    private int mDay;

    private TextView mDateDisplay;
    private Button mPickDate;

    static final int DATE_DIALOG_ID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datapembeli);

        //--------------Intent
        final Intent intent = getIntent();

        //-----------------DatePicker

        mDateDisplay = (TextView) findViewById(R.id.pembeli_tanggal);
        mPickDate = (Button) findViewById(R.id.btn_date);

        mPickDate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        // get the current date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        // display the current date
        updateDisplay();


        //------------API

        //Building Retrofit Rest Adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                //set the path
                .setEndpoint(MainActivity.API)
                        //SSL Certificate for HTTPS
                .setClient(new OkClient(SelfSigningClient.createClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        //API Service
        final PropertiInterface properti = restAdapter.create(PropertiInterface.class);

        //-------------------Spinner
        final Spinner dropdown = (Spinner) findViewById(R.id.pembeli_jenis);
        String[] items = new String[]{"Tunai Keras", "Tunai Bertahap", "KPR"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        //---------------------Submit

        Button submit = (Button) findViewById(R.id.btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText et_nama = (EditText) findViewById(R.id.pembeli_nama);
                EditText et_ktp = (EditText) findViewById(R.id.pembeli_ktp);
                EditText et_hp = (EditText) findViewById(R.id.pembeli_hp);
                EditText et_tempat = (EditText) findViewById(R.id.pembeli_tempat);
                TextView tv_tanggal = (TextView) findViewById(R.id.pembeli_tanggal);

                properti.createPembeli(
                        et_nama.getText().toString(),
                        et_ktp.getText().toString(),
                        et_hp.getText().toString(),
                        et_tempat.getText().toString(),
                        tv_tanggal.getText().toString(),
                        new Callback<Pembeli>() {
                            @Override
                            public void success(Pembeli pembeli, Response response) {
                                float booked = (float) 0.00;
                                CheckBox check = (CheckBox) findViewById(R.id.pembeli_book);
                                if (check.isChecked()) {
                                    booked = Float.parseFloat(intent.getStringExtra("HargaRumah")) / 10;
                                }

                                properti.createPembelian(
                                        intent.getStringExtra("IDRumah"),
                                        pembeli.getIDPembeli(),
                                        dropdown.getSelectedItem().toString(),
                                        intent.getStringExtra("HargaRumah"),
                                        Float.toString(booked),
                                        intent.getStringExtra("Username"),
                                        new SimpleDateFormat("yyyy-MM-dd").format(new Date()),
                                        new Callback<Pembelian>() {

                                            @Override
                                            public void success(Pembelian pembelian, Response response) {
                                                Toast.makeText(getBaseContext(), "IDPembeli anda adalah : "+pembelian.getIDPembelian(), Toast.LENGTH_LONG).show();
                                                finish();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                Toast.makeText(getBaseContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                );
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(getBaseContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                );
            }
        });
    }

    private void updateDisplay() {
        this.mDateDisplay.setText(
                new StringBuilder()
                        // Month is 0 based so add 1
                        .append(mYear).append("-")
                        .append(mMonth + 1).append("-")
                        .append(mDay));
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    updateDisplay();
                }
            };

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this,
                        mDateSetListener,
                        mYear, mMonth, mDay);
        }
        return null;
    }

}
