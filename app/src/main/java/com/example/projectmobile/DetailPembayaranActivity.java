package com.example.projectmobile;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.projectmobile.Certificate.SelfSigningClient;
import com.example.projectmobile.Interface.PropertiInterface;
import com.example.projectmobile.Model.Pembelian;
import com.example.projectmobile.Model.PembelianDetail;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;


public class DetailPembayaranActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_pembayaran);

        final EditText et_id = (EditText) findViewById(R.id.detail_pembelian);
        final EditText et_biaya = (EditText) findViewById(R.id.detail_biaya);
        final EditText et_ket = (EditText) findViewById(R.id.detail_keterangan);

        Button btn_pay = (Button) findViewById(R.id.btn_pay);
        Button btn_exit = (Button) findViewById(R.id.btn_exit);
        Button btn_cancel = (Button) findViewById(R.id.btn_cancel);


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(MainActivity.API)
                .setClient(new OkClient(SelfSigningClient.createClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        final PropertiInterface properti = restAdapter.create(PropertiInterface.class);


        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                properti.postPembelian(
                        et_id.getText().toString(),
                        et_biaya.getText().toString(),
                        et_ket.getText().toString(),
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),
                        new Callback<PembelianDetail>() {
                            @Override
                            public void success(PembelianDetail pembelianDetail, Response response) {
                                Toast.makeText(getBaseContext(), "Data berhasil dimasukkan", Toast.LENGTH_LONG);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(getBaseContext(), error.getMessage(), Toast.LENGTH_LONG);
                            }
                        });
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                properti.deleteBooking(et_id.getText().toString(), new Callback<Pembelian>() {
                    @Override
                    public void success(Pembelian pembelian, Response response) {
                        Toast.makeText(getBaseContext(), "Booking telah dicancel", Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getBaseContext(), error.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });
            }
        });

        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
