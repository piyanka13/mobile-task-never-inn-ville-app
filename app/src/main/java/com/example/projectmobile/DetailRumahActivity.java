package com.example.projectmobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmobile.Adapter.RumahAdapter;
import com.example.projectmobile.Certificate.SelfSigningClient;
import com.example.projectmobile.Interface.PropertiInterface;
import com.example.projectmobile.Model.Pembelian;
import com.example.projectmobile.Model.Properti;
import com.example.projectmobile.Model.Rumah;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class DetailRumahActivity extends AppCompatActivity {
    List<Rumah> rumahList = new ArrayList<Rumah>();
    List<String> IdRumah = new ArrayList<String>();
    List<Pembelian> pembelianList= new ArrayList<Pembelian>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_rumah);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //----------------API Service with Rest
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(MainActivity.API)
                .setClient(new OkClient(SelfSigningClient.createClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        final PropertiInterface properti = restAdapter.create(PropertiInterface.class);
        final Intent intent = getIntent();
        final ListView list_nomor = (ListView) findViewById(R.id.list_rumah_nomor);

        //-----------------get Rumah detail based on tipe
        properti.searchRumah(intent.getStringExtra("TipeRumah"), new Callback<List<Rumah>>() {
            @Override
            public void success(List<Rumah> rumahs, Response response) {
                rumahList = rumahs;
                for (int i = 0; i < rumahs.size(); i++) {
                    IdRumah.add(rumahs.get(i).getIDRumah());
                }

                //-------------------get Pembelian based on IDRumah
                properti.checkPembelian(IdRumah, new Callback<List<Pembelian>>() {
                    @Override
                    public void success(List<Pembelian> pembelians, Response response) {
                        RumahAdapter ra = new RumahAdapter(getBaseContext(), R.layout.list_rumah_nomor, rumahList, pembelians);
                        pembelianList = pembelians;
                        list_nomor.setAdapter(ra);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        RumahAdapter ra = new RumahAdapter(getBaseContext(), R.layout.list_rumah_nomor, rumahList, null);
                        list_nomor.setAdapter(ra);
                        Toast.makeText(getBaseContext(),error.getResponse().getStatus()+" : "+error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getBaseContext(),error.getResponse().getStatus()+" : "+error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        //--------------Ketika Nomor ditekan
        list_nomor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //------------------------PopUp
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                final View popupView = layoutInflater.inflate(R.layout.popup_nomor, null);

                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                );
                TextView tv_nama_properti = (TextView) popupView.findViewById(R.id.properti_nama);
                TextView tv_rumah_tipe = (TextView) popupView.findViewById(R.id.rumah_tipe);
                TextView tv_rumah_nomor = (TextView) popupView.findViewById(R.id.rumah_nomor);
                TextView tv_rumah_harga = (TextView) popupView.findViewById(R.id.rumah_harga);

                tv_nama_properti.setText("Kompleks " + intent.getStringExtra("NamaProperti"));
                tv_rumah_tipe.setText("Tipe : " + rumahList.get(position).getTipeRumah());
                tv_rumah_nomor.setText("Nomor : " + rumahList.get(position).getNoRumah());
                tv_rumah_harga.setText("Harga : " + rumahList.get(position).getHargaJualDasar());

                //---------Change view if available to buy
                TextView tv_clarify = (TextView) popupView.findViewById(R.id.label_clarification);
                Button add = (Button) popupView.findViewById(R.id.btn_request);

                for (Pembelian p : pembelianList) {
                    if (p != null && rumahList.get(position).getIDRumah().contains(p.getIDRumah())) {
                        add.setVisibility(View.GONE);
                        tv_clarify.setVisibility(View.GONE);
                    }
                }


                final String string_id = rumahList.get(position).getIDRumah();
                final String string_harga = rumahList.get(position).getHargaJualDasar();

                //-----------------Cancel
                Button dismiss = (Button) popupView.findViewById(R.id.btn_cancel);
                dismiss.setOnClickListener(new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });

                //-----------------Add Properti
                add.setOnClickListener(new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent_pembeli = new Intent(getBaseContext(), datapembeli.class);
                        intent_pembeli.putExtra("IDRumah", string_id);
                        intent_pembeli.putExtra("HargaRumah", string_harga);
                        intent_pembeli.putExtra("Username", intent.getStringExtra("Username"));
                        startActivity(intent_pembeli);
                    }
                });

                popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

            }
        });


    }

}
