package com.example.projectmobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmobile.Adapter.PropertiAdapter;
import com.example.projectmobile.Adapter.RumahAdapter;
import com.example.projectmobile.Adapter.RumahExpandAdapter;
import com.example.projectmobile.Certificate.SelfSigningClient;
import com.example.projectmobile.Interface.PropertiInterface;
import com.example.projectmobile.Model.Properti;
import com.example.projectmobile.Model.Rumah;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;



public class DetailPropertiActivity extends AppCompatActivity {

    //expandlist
    RumahExpandAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    String namaProperti;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.properti_detail);

        //--------------------------Intent
        final Intent i = getIntent();
        final String product = i.getStringExtra("id");


        //Building Retrofit Rest Adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                //set the path
                .setEndpoint(MainActivity.API)
                        //SSL Certificate for HTTPS
                .setClient(new OkClient(SelfSigningClient.createClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        //API Service
        final PropertiInterface properti = restAdapter.create(PropertiInterface.class);

        final TextView tv_id = (TextView) findViewById(R.id.properti_id);

        //Calling API Service Function
        properti.getDetail(product, new Callback<Properti>() {

            //If Successful
            @Override
            public void success(Properti users, Response response) {
                TextView tv_nama = (TextView) findViewById(R.id.properti_nama);
                TextView tv_lokasi = (TextView) findViewById(R.id.properti_lokasi);
                TextView tv_unit = (TextView) findViewById(R.id.properti_unit);

                tv_id.setText(users.getIDProperti().toString());
                tv_nama.setText(users.getNamaProyek());
                tv_lokasi.setText("Lokasi : " + users.getLokasi());
                tv_unit.setText("Jumlah Unit : " + users.getJumlahUnitTotal().toString());

                namaProperti = users.getNamaProyek();

            }

            //If Failed
            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getBaseContext(),error.getResponse().getStatus()+" : "+error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        properti.searchTipeRumah(product, new Callback<List<Rumah>>() {
            public void success(List<Rumah> rumahs, Response response) {

                //expandable list
                expListView = (ExpandableListView) findViewById(R.id.list_rumah);
                prepareListData(rumahs);
                listAdapter = new RumahExpandAdapter(getApplicationContext(), listDataHeader, listDataChild, namaProperti, i.getStringExtra("Username"));
                expListView.setAdapter(listAdapter);

            }

            public void failure(RetrofitError error) {
                Toast.makeText(getBaseContext(), error.getResponse().getStatus() + " : " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


        final Button add_rumah = (Button) findViewById(R.id.btn_add);
        add_rumah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //popup
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                final View popupView = layoutInflater.inflate(R.layout.popup_rumah, null);

                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                );
                //popup input
                popupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
                popupWindow.setFocusable(true);
                popupWindow.setOutsideTouchable(false);
                popupWindow.update();

                //cancel button
                Button dismiss = (Button) popupView.findViewById(R.id.btn_cancel);
                dismiss.setOnClickListener(new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });

                //add properti
                Button add = (Button) popupView.findViewById(R.id.btn_add);
                add.setOnClickListener(new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText et_tipe = (EditText) popupView.findViewById(R.id.et_tipe);
                        EditText et_harga = (EditText) popupView.findViewById(R.id.et_harga);
                        EditText et_ukuran = (EditText) popupView.findViewById(R.id.et_ukuran);
                        EditText et_jumlah = (EditText) popupView.findViewById(R.id.et_jumlah);

                        String post = "[";
                        for(int i = 1; i <= Integer.parseInt(et_jumlah.getText().toString()); i++   ) {
                            properti.postRumah(
                                    product,
                                    String.valueOf(i),
                                    et_tipe.getText().toString(),
                                    et_harga.getText().toString(),
                                    et_ukuran.getText().toString(),
                                    new Callback<Rumah>() {

                                        @Override
                                        public void success(Rumah rumah, Response response) {
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                        }
                                    });
                        }
                        Intent in = getIntent();
                        finish();
                        startActivity(in);
                    }
                });

                popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

            }
        });



    }

    //expand list
    private void prepareListData(List<Rumah> rumahs) {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();


        List<String> child = new ArrayList<String>();

        // Adding header data
        for(int i = 0; i< rumahs.size(); i++){
            listDataHeader.add(rumahs.get(i).getTipeRumah());
            child = new ArrayList<String>();
            child.add("Ukuran : "+rumahs.get(i).getUkuranTipe());

            listDataChild.put(listDataHeader.get(i), child);
        }

    }



}
