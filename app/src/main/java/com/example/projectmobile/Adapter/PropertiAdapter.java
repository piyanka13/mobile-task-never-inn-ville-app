package com.example.projectmobile.Adapter;

import android.content.Context;
import android.opengl.Visibility;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmobile.Model.Properti;
import com.example.projectmobile.R;

import java.util.List;



public class PropertiAdapter extends ArrayAdapter<Properti> {
    String[] userid;
    String[] dispname;
    Context context;
    String[] result;
    private static LayoutInflater inflater = null;

    public PropertiAdapter(Context context, int resource, List<Properti> users) {
        super(context, resource, users);
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_properti, null);
        }

        Properti p = getItem(position);

        if (p != null) {
            TextView tv_id = (TextView) v.findViewById(R.id.properti_id);
            TextView tv_nama = (TextView) v.findViewById(R.id.properti_nama);
            TextView tv_lokasi = (TextView) v.findViewById(R.id.properti_lokasi);
            TextView tv_unit = (TextView) v.findViewById(R.id.properti_unit);

            if (tv_id != null) {
                tv_id.setText(p.getIDProperti().toString());
            }

            if (tv_nama != null) {
                tv_nama.setText(p.getNamaProyek());
            }

            if (tv_lokasi != null) {
                tv_lokasi.setText(p.getLokasi());
            }
            if (tv_unit != null) {
                tv_unit.setText(p.getJumlahUnitTotal());
            }

        }

        return v;
    }
}
