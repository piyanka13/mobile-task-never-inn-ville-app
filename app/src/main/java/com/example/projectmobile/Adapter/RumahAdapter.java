package com.example.projectmobile.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmobile.Model.Pembelian;
import com.example.projectmobile.Model.Properti;
import com.example.projectmobile.Model.Rumah;
import com.example.projectmobile.R;

import java.util.List;


public class RumahAdapter extends ArrayAdapter<Rumah> {
    String[] id;
    String[] id_prop;
    String[] no;
    String[] tipe;
    String[] harga;
    String[] ukuran;
    List<Pembelian> pembelianList;

    String[] result;
    Context context;
    private static LayoutInflater inflater = null;

    public RumahAdapter(Context context, int resource, List<Rumah> rumah) {
        super(context, resource, rumah);
    }

    public RumahAdapter(Context context, int resource, List<Rumah> rumah, List<Pembelian> pembelian){
        super(context, resource, rumah);
        pembelianList = pembelian;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater vi;
        vi = LayoutInflater.from(getContext());
        View v = convertView;
        View v_pop = vi.inflate(R.layout.popup_nomor, null);;

        if (v == null) {
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_rumah_nomor, null);
        }


        Rumah p = getItem(position);

        if (p != null) {
            TextView tv_id = (TextView) v.findViewById(R.id.rumah_id);
            TextView tv_id_prop = (TextView) v.findViewById(R.id.rumah_id_prop);
            TextView tv_no = (TextView) v.findViewById(R.id.rumah_nomor);
            TextView tv_tipe = (TextView) v.findViewById(R.id.rumah_tipe);
            TextView tv_harga = (TextView) v.findViewById(R.id.rumah_harga);
            TextView tv_ukuran = (TextView) v.findViewById(R.id.rumah_ukuran);

            if (tv_id != null)
                tv_id.setText(p.getIDRumah());
            if (tv_id_prop != null)
                tv_id_prop.setText(p.getIDProperti());

            if (tv_no != null) {
                tv_no.setText(p.getNoRumah());

                //---------------memilih gambar
                boolean available = true;
                ImageView logo = (ImageView) v.findViewById(R.id.nomor_logo);
                if(pembelianList != null) {
                    for (int i = 0; i < pembelianList.size(); i++) {
                        if (p.getIDRumah().equals(pembelianList.get(i).getIDRumah())) {
                            if (pembelianList.get(i).getJenisPembelian().equals("Tunai Bertahap")
                                    && Float.parseFloat(pembelianList.get(i).getBookingFee()) > 0.00) {
                                logo.setImageResource(R.drawable.logo_booked);
                            } else {
                                logo.setImageResource(R.drawable.logo_paid);
                            }
                            available = false;
                            break;
                        }
                    }
                    if (available) {
                        logo.setImageResource(R.drawable.logo_available);
                    }
                }else{
                    logo.setImageResource(R.drawable.logo_available);
                }

            }
            if (tv_tipe != null) {
                tv_tipe.setText(p.getTipeRumah());
                tv_tipe.setVisibility(View.VISIBLE);
            }
            if (tv_harga != null)
                tv_harga.setText(p.getHargaJualDasar());
            if (tv_ukuran != null)
                tv_ukuran.setText(p.getUkuranTipe());
        }

        return v;
    }
}
