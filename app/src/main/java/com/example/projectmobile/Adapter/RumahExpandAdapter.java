package com.example.projectmobile.Adapter;

import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.provider.SyncStateContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmobile.DetailPropertiActivity;
import com.example.projectmobile.DetailRumahActivity;
import com.example.projectmobile.R;
import com.example.projectmobile.accounting_home_activity;

public class RumahExpandAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;
    private String namaProperti;
    private String username;

    public RumahExpandAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<String>> listChildData, String namaProperti, String username) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.namaProperti = namaProperti;
        this.username = username;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, final ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_rumah, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.rumah_ukuran);

        txtListChild.setText(childText);

        Button detail_rumah = (Button) convertView.findViewById(R.id.detail_rumah_nomor);
        detail_rumah.setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        _context.startActivity(new Intent(_context, DetailRumahActivity.class)
                                .putExtra("TipeRumah", _listDataHeader.get(groupPosition))
                                .putExtra("NamaProperti", namaProperti)
                                .putExtra("Username", username)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)); ;
                    }
                }
        );
        detail_rumah.setFocusable(false);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.header_rumah, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.header_rumah);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}