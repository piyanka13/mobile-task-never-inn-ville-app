package com.example.projectmobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmobile.Adapter.User;
import com.example.projectmobile.Certificate.SelfSigningClient;
import com.example.projectmobile.Interface.PropertiInterface;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class LoginActivity extends AppCompatActivity {

    Button b_login;
    EditText txt_email, txt_passwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);
        TextView registerScreen = (TextView) findViewById(R.id.linkregis);
        registerScreen.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
            }
        });

        b_login = (Button) findViewById(R.id.btnlogin);
        txt_email = (EditText) findViewById(R.id.email);
        txt_passwd = (EditText) findViewById(R.id.password);
        b_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Building Retrofit Rest Adapter
                RestAdapter restAdapter = new RestAdapter.Builder()
                        //set the path
                        .setEndpoint(MainActivity.API)
                                //SSL Certificate for HTTPS
                        .setClient(new OkClient(SelfSigningClient.createClient()))
                        .setLogLevel(RestAdapter.LogLevel.FULL)
                        .build();

                //API Service
                PropertiInterface git = restAdapter.create(PropertiInterface.class);

                //Calling API Service Function
                git.getLogin(txt_email.getText().toString(), new Callback<List<User>>() {

                    //If Successful
                    @Override
                    public void success(List<User> user, Response response) {
                        boolean success = false;
                        for (int j = 0; j < user.size(); j++) {
                            //success!
                            if (txt_email.getText().toString().equals(user.get(j).getUsername())
                                    && txt_passwd.getText().toString().equals(user.get(j).getPassword().toString()))
                                success = true;
                            Intent i;
                            //keuangan
                            if (user.get(j).getUsername().equals("finance")) {
                                i = new Intent(getApplicationContext(), accounting_home_activity.class);
                            } else if (user.get(j).getUsername().equals("developer")) {
                                i = new Intent(getApplicationContext(), DeveloperActivity.class);
                            } else {
                                i = new Intent(getApplicationContext(), MainActivity.class);
                            }
                            i.putExtra("username", user.get(j).getDisplayName());
                            i.putExtra("id", user.get(j).getUserID());
                            startActivity(i);
                        }
                        if (!success)
                            Toast.makeText(getApplicationContext(), "Username or password is wrong", Toast.LENGTH_SHORT).show();
                        else
                            finish();
                    }

                    //If Failed
                    @Override
                    public void failure(RetrofitError error) {
                        if (error.getMessage().equals("404 Not Found")) {
                            Toast.makeText(getApplicationContext(), "Username doesn't exist", Toast.LENGTH_SHORT).show();
                        } else {
                            TextView tv = (TextView) findViewById(R.id.linkregis);
                            tv.setText(error.getMessage());
                        }
                    }
                });

            }
        });


    }

}
