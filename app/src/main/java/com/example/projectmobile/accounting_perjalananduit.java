package com.example.projectmobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TableLayout;

import com.example.projectmobile.Certificate.SelfSigningClient;
import com.example.projectmobile.Interface.PropertiInterface;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class accounting_perjalananduit extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounting_perjalananduit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Intent intent = getIntent();

        //Building Retrofit Rest Adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                //set the path
                .setEndpoint(MainActivity.API)
                        //SSL Certificate for HTTPS
                .setClient(new OkClient(SelfSigningClient.createClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        //API Service
        final PropertiInterface git = restAdapter.create(PropertiInterface.class);

        final TableLayout tableLayout = (TableLayout) findViewById(R.id.table_keuangan);
    /*
        git.getPembelianDetail(new Callback<List<PembelianDetail>>() {
            @Override
            public void success(final List<PembelianDetail> pembelians, Response response) {
                git.getBiayaProyek(new Callback<List<BiayaProyek>>(){

                    @Override
                    public void success(List<BiayaProyek> biayaProyeks, Response response) {
                        int i_pembeli = 0, j_proyek =  0;
                        BigDecimal total = new BigDecimal("0");
                        while(i_pembeli < pembelians.size() || j_proyek < biayaProyeks.size()) {
                            boolean profit = false;
                            try {
                                Date pembeli = null, proyek = null;
                                if(i_pembeli != pembelians.size())
                                    pembeli = (Date) new SimpleDateFormat("yyyy-MM-dd").parse(pembelians.get(i_pembeli).getTanggalBayar());
                                else
                                    profit = false;

                                if(j_proyek !=biayaProyeks.size())
                                    proyek = (Date) new SimpleDateFormat("yyyy-MM-dd").parse(biayaProyeks.get(j_proyek).getTanggal());
                                else
                                    profit = true;
                                if(pembeli != null && proyek != null) {
                                    if(pembeli.after(proyek))
                                    profit = true;
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            TableRow tableRow = new TableRow(getBaseContext());
                            TextView tv = new TextView(getBaseContext());
                            TextView tv2 = new TextView(getBaseContext());
                            TextView tv3 = new TextView(getBaseContext());

                            if(profit) {
                                tv.setText(pembelians.get(i_pembeli).getJumlah());
                                tv2.setText(pembelians.get(i_pembeli).getTanggalBayar());
                                tv3.setText(pembelians.get(i_pembeli).getKeterangan());
                                total = total.add(new BigDecimal(pembelians.get(i_pembeli).getJumlah()));
                                tv.setTextColor(Color.GREEN);
                                tv2.setTextColor(Color.GREEN);
                                tv3.setTextColor(Color.GREEN);
                                i_pembeli++;
                            }else{
                                tv.setText(biayaProyeks.get(j_proyek).getBiaya());
                                tv2.setText(biayaProyeks.get(j_proyek).getTanggal());
                                tv3.setText(biayaProyeks.get(j_proyek).getKeterangan());
                                total = total.subtract(new BigDecimal(biayaProyeks.get(j_proyek).getBiaya()));
                                tv.setTextColor(Color.RED);
                                tv2.setTextColor(Color.RED);
                                tv3.setTextColor(Color.RED);
                                j_proyek++;
                            }
                            tableRow.addView(tv2);
                            tableRow.addView(tv);
                            tableRow.addView(tv3);
                            tv.setShadowLayer(1, 1, 1, Color.BLACK);
                            tv2.setShadowLayer(1, 1, 1, Color.BLACK);
                            tv3.setShadowLayer(1, 1, 1, Color.BLACK);
                            tv.setPadding(10, 10, 10, 10);
                            tv2.setPadding(10, 10, 10, 10);
                            tv3.setPadding(10, 10, 10, 10);
                            tableLayout.addView(tableRow);
                        }
                        TextView tv_total = (TextView) findViewById(R.id.label_title);
                        if(total.signum() == 1){
                            tv_total.setText("Surplus : "+total.toString());
                            tv_total.setTextColor(Color.GREEN);
                        }else if(total.signum() == -1){
                            tv_total.setText("Defisit : "+total.toString());
                            tv_total.setTextColor(Color.RED);
                        }else{
                            tv_total.setText("Uang saat ini : 0.00");
                            tv_total.setTextColor(Color.BLACK);
                        }
                        tv_total.setShadowLayer(1, 1, 1, Color.BLACK);
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
        */
        /*

        Intent i = getIntent();
        String i_nama = i.getStringExtra("nama");
        String i_id = i.getStringExtra("id");

        TextView tv = (TextView) findViewById(R.id.textView2);
        tv.setText(i_nama);
        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */

    }
}
