package com.example.projectmobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmobile.Adapter.User;
import com.example.projectmobile.Certificate.SelfSigningClient;
import com.example.projectmobile.Interface.PropertiInterface;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class RegisterActivity extends AppCompatActivity {

    Button b2;
    EditText txt1,txt2,txt3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_page);
        TextView t1 = (TextView)findViewById(R.id.link_login);
        t1.setOnClickListener(new View.OnClickListener() {
                                  public void onClick(View v) {
                                      finish();
                                  }
                              }
        );

        Button button = (Button) findViewById(R.id.btnregister);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText tv_dispname = (EditText) findViewById(R.id.reg_name);
                EditText tv_username = (EditText) findViewById(R.id.reg_email);
                EditText tv_passwd = (EditText) findViewById(R.id.reg_password);

                //Building Retrofit Rest Adapter
                RestAdapter restAdapter = new RestAdapter.Builder()
                        //set the path
                        .setEndpoint(MainActivity.API)
                                //SSL Certificate for HTTPS
                        .setClient(new OkClient(SelfSigningClient.createClient()))
                        .setLogLevel(RestAdapter.LogLevel.FULL)
                        .build();

                //API Service
                PropertiInterface git = restAdapter.create(PropertiInterface.class);

                //Calling API Service Function
                git.createLogin(
                        tv_username.getText().toString(),
                        tv_passwd.getText().toString(),
                        tv_dispname.getText().toString(),
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),
                        new Callback<User>() {

                    //If Successful
                    @Override
                    public void success(User user, Response response) {
                        Toast.makeText(getBaseContext(), "User berhasil ditambahkan", Toast.LENGTH_LONG);
                        finish();
                    }

                    //If Failed
                    @Override
                    public void failure(RetrofitError error) {
                    }
                });
            }
        });


    }

}
