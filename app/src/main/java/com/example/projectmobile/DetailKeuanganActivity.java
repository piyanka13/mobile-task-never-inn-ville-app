package com.example.projectmobile;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmobile.Adapter.RumahAdapter;
import com.example.projectmobile.Certificate.SelfSigningClient;
import com.example.projectmobile.Interface.PropertiInterface;
import com.example.projectmobile.Model.BiayaProyek;
import com.example.projectmobile.Model.Pembelian;
import com.example.projectmobile.Model.PembelianDetail;
import com.example.projectmobile.Model.Rumah;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;


public class DetailKeuanganActivity extends Activity {
    List<String> IdRumah = new ArrayList<String>();
    List<String> IdPembelian = new ArrayList<String>();
    List<BiayaProyek> BiayaProyekList = new ArrayList<BiayaProyek>();
    List<PembelianDetail> PembelianDetailList = new ArrayList<PembelianDetail>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounting_perjalananduit);

        //----------------API Service with Rest
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(MainActivity.API)
                .setClient(new OkClient(SelfSigningClient.createClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        final PropertiInterface properti = restAdapter.create(PropertiInterface.class);
        final Intent intent = getIntent();

        final TableLayout tableLayout = (TableLayout) findViewById(R.id.table_keuangan);

        //Get Biaya Proyek

        //Get Pembelian Detail
        properti.getRumahProperti(intent.getStringExtra("IDProperti"), new Callback<List<Rumah>>() {
            @Override
            public void success(List<Rumah> rumahs, Response response) {
                for (int i = 0; i < rumahs.size(); i++) {
                    IdRumah.add(rumahs.get(i).getIDRumah());
                }
                //-------------------get Pembelian based on IDRumah
                properti.checkPembelian(IdRumah, new Callback<List<Pembelian>>() {
                    @Override
                    public void success(List<Pembelian> pembelians, Response response) {
                        for (int i = 0; i < pembelians.size(); i++) {
                            IdPembelian.add(pembelians.get(i).getIDPembelian());
                        }
                        properti.getPembelianDetail(IdPembelian, new Callback<List<PembelianDetail>>() {
                            @Override
                            public void success(final List<PembelianDetail> pembelianDetails, Response response) {
                                PembelianDetailList = pembelianDetails;
                                properti.getBiayaProyek(intent.getStringExtra("IDProperti"), new Callback<List<BiayaProyek>>() {
                                    @Override
                                    public void success(List<BiayaProyek> biayaProyeks, Response response) {
                                        BiayaProyekList = biayaProyeks;
                                        if (PembelianDetailList != null && BiayaProyekList != null) {
                                        int i_pembeli = 0, j_proyek = 0;
                                        BigDecimal total = new BigDecimal("0");
                                        while (i_pembeli < PembelianDetailList.size() || j_proyek < BiayaProyekList.size()) {
                                            boolean profit = false;
                                            try {
                                                Date pembeli = null, proyek = null;
                                                if (i_pembeli != PembelianDetailList.size())
                                                    pembeli = (Date) new SimpleDateFormat("yyyy-MM-dd").parse(PembelianDetailList.get(i_pembeli).getTanggalBayar());
                                                else
                                                    profit = false;

                                                if (j_proyek != BiayaProyekList.size())
                                                    proyek = (Date) new SimpleDateFormat("yyyy-MM-dd").parse(BiayaProyekList.get(j_proyek).getTanggal());
                                                else
                                                    profit = true;
                                                if (pembeli != null && proyek != null) {
                                                    if (pembeli.after(proyek))
                                                        profit = true;
                                                }
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                            TableRow tableRow = new TableRow(getBaseContext());
                                            TextView tv = new TextView(getBaseContext());
                                            TextView tv2 = new TextView(getBaseContext());
                                            TextView tv3 = new TextView(getBaseContext());

                                            if (profit) {
                                                tv.setText(PembelianDetailList.get(i_pembeli).getJumlah());
                                                tv2.setText(PembelianDetailList.get(i_pembeli).getTanggalBayar());
                                                tv3.setText(PembelianDetailList.get(i_pembeli).getKeterangan());
                                                total = total.add(new BigDecimal(PembelianDetailList.get(i_pembeli).getJumlah()));
                                                tv.setTextColor(Color.GREEN);
                                                tv2.setTextColor(Color.GREEN);
                                                tv3.setTextColor(Color.GREEN);
                                                i_pembeli++;
                                            } else {
                                                tv.setText(BiayaProyekList.get(j_proyek).getBiaya());
                                                tv2.setText(BiayaProyekList.get(j_proyek).getTanggal());
                                                tv3.setText(BiayaProyekList.get(j_proyek).getKeterangan());
                                                total = total.subtract(new BigDecimal(BiayaProyekList.get(j_proyek).getBiaya()));
                                                tv.setTextColor(Color.RED);
                                                tv2.setTextColor(Color.RED);
                                                tv3.setTextColor(Color.RED);
                                                j_proyek++;
                                            }
                                            tableRow.addView(tv2);
                                            tableRow.addView(tv);
                                            tableRow.addView(tv3);
                                            tv.setShadowLayer(1, 1, 1, Color.BLACK);
                                            tv2.setShadowLayer(1, 1, 1, Color.BLACK);
                                            tv3.setShadowLayer(1, 1, 1, Color.BLACK);
                                            tv.setPadding(10, 10, 10, 10);
                                            tv2.setPadding(10, 10, 10, 10);
                                            tv3.setPadding(10, 10, 10, 10);
                                            tableLayout.addView(tableRow);
                                        }
                                        TextView tv_total = (TextView) findViewById(R.id.label_title);
                                        if (total.signum() == 1) {
                                            tv_total.setText("Surplus : " + total.toString());
                                            tv_total.setTextColor(Color.GREEN);
                                        } else if (total.signum() == -1) {
                                            tv_total.setText("Defisit : " + total.toString());
                                            tv_total.setTextColor(Color.RED);
                                        } else {
                                            tv_total.setText("Uang saat ini : 0.00");
                                            tv_total.setTextColor(Color.BLACK);
                                        }
                                        tv_total.setShadowLayer(1, 1, 1, Color.BLACK);
                                    }

                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Toast.makeText(getBaseContext(), error.getResponse().getStatus() + " : " + error.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                });
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(getBaseContext(), error.getResponse().getStatus() + " : " + error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getBaseContext(), error.getResponse().getStatus() + " : " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getBaseContext(), error.getResponse().getStatus() + " : " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
