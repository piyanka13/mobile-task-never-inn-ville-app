package com.example.projectmobile.Interface;

import com.example.projectmobile.Adapter.User;
import com.example.projectmobile.Model.BiayaProyek;
import com.example.projectmobile.Model.NamaBiaya;
import com.example.projectmobile.Model.Pembeli;
import com.example.projectmobile.Model.Pembelian;
import com.example.projectmobile.Model.PembelianDetail;
import com.example.projectmobile.Model.Properti;
import com.example.projectmobile.Model.Rumah;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;



//For API Service

public interface PropertiInterface {

    //GET Methods
    @GET("/properties")
    void getFeed(Callback<List<Properti>> response);

    @GET("/properties/{id}")
    void getDetail(@Path("id") String id, Callback<Properti> response);


    @FormUrlEncoded
    @POST("/properties")
    void addProperti(@Field("NamaProyek") String np,
                     @Field("Lokasi") String lokasi,
                     @Field("JumlahUnitTotal") String jlh,
                     Callback<Properti> response);


    //search login user
    @GET("/users/search")
    void getLogin(@Query("Username") String username, Callback<List<User>> response);

    //register user
    @FormUrlEncoded
    @POST("/users")
    void createLogin(@Field("Username") String username,
                     @Field("Password") String password,
                     @Field("DisplayName") String dispname,
                     @Field("LastLoggedIn") String login,
                     Callback<User> response);

    @GET("/rumahs")
    void getRumah(Callback<List<Rumah>> response);

    @GET("/rumahs/search")
    void getRumahProperti(@Query("IDProperti") String id, Callback<List<Rumah>> response);

    @GET("/rumahs/distinct")
    void searchTipeRumah(@Query("IDProperti") String id, Callback<List<Rumah>> response);

    @GET("/rumahs/nomor")
    void searchRumah(@Query("TipeRumah") String tipe, Callback<List<Rumah>> response);

    //get pembelian with multiple id
    @GET("/pembelians/check")
    void checkPembelian(@Query("IDRumah[]") List<String> id, Callback<List<Pembelian>> response);

    //post pembeli
    @FormUrlEncoded
    @POST("/pembelis")
    void createPembeli(@Field("NamaPembeli") String nama,
                       @Field("NoKTP") String ktp,
                       @Field("NoHP") String hp,
                       @Field("TempatLahir") String tempat,
                       @Field("TanggalLahir") String tanggal,
                       Callback<Pembeli> response);

    //post pembelian
    @FormUrlEncoded
    @POST("/pembelians")
    void createPembelian(@Field("IDRumah") String idr,
                         @Field("IDPembeli") String idp,
                         @Field("JenisPembelian") String jenis,
                         @Field("HargaBeli") String harga,
                         @Field("BookingFee") String book,
                         @Field("NamaMarketing") String marketing,
                         @Field("TanggalBooking") String tanggal,
                        Callback<Pembelian> response
    );



    //get namabiaya
    @GET("/namabiayas")
    void getNamaBiaya(Callback<List<NamaBiaya>> response);

    //post biaya proyek
    @FormUrlEncoded
    @POST("/biayaproyeks")
    void postBiayaProyek(@Field("IDProperti") String idp,
                         @Field("IDNamaBiaya") String idn,
                         @Field("NamaBiayaDetail") String nbd,
                         @Field("Keterangan") String ket,
                         @Field("Biaya") String biaya,
                         @Field("Tanggal") String tgl,
                         Callback<BiayaProyek> response
    );

    //get all pembelian
    @GET("/pembeliandetails/date")
    void getPembelianDetail(@Query("IDPembelian[]") List<String> id, Callback<List<PembelianDetail>> response);

    //get all biayaproyek
    @GET("/biayaproyeks/date")
    void getBiayaProyek(@Query("IDProperti") String id, Callback<List<BiayaProyek>> response);

    //Delete properti
    @DELETE("/properties/{id}")
    void deleteProperti(@Path("id") String id, Callback<Properti> response);

    @FormUrlEncoded
    @POST("/rumahs")
    void postRumah(@Field("IDProperti") String id,
                   @Field("NoRumah") String no,
                   @Field("TipeRumah") String tipe,
                   @Field("HargaJualDasar")String harga,
                   @Field("UkuranTipe") String ukuran,
                   Callback<Rumah> response);

    @FormUrlEncoded
    @POST("/pembeliandetails")
    void postPembelian(
            @Field("IDPembelian") String id,
            @Field("Jumlah") String jlh,
            @Field("Keterangan") String ket,
            @Field("TanggalBayar") String tanggal,
            Callback<PembelianDetail> response);

    @DELETE("/pembelians/{id}")
    void deleteBooking(@Path("id") String id,
                       Callback<Pembelian> response);

    @DELETE("/rumahs/search")
    void deleteTipeRumah(@Query("TipeRumah") String id,
                         Callback<Rumah> response);
}
