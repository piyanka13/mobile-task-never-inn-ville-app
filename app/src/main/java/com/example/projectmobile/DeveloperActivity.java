package com.example.projectmobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmobile.Adapter.PropertiAdapter;
import com.example.projectmobile.Certificate.SelfSigningClient;
import com.example.projectmobile.Interface.PropertiInterface;
import com.example.projectmobile.Model.Properti;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class DeveloperActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{
    public String username;
    public String id;
    public List<Properti> propertiList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */

        Intent i_in = getIntent();
        username = i_in.getStringExtra("username");
        id = i_in.getStringExtra("id");


        //Building Retrofit Rest Adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                //set the path
                .setEndpoint(MainActivity.API)
                        //SSL Certificate for HTTPS
                .setClient(new OkClient(SelfSigningClient.createClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        //API Service
        final PropertiInterface git = restAdapter.create(PropertiInterface.class);

        final ListView listView = (ListView) findViewById(R.id.list_properti);

        //Calling API Service Function
        git.getFeed(new Callback<List<Properti>>() {

            //If Successful
            @Override
            public void success(List<Properti> users, Response response) {
                listView.setAdapter(new PropertiAdapter(getBaseContext(), R.layout.list_properti, users));
                propertiList = users;
            }

            //If Failed
            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getBaseContext(), error.getResponse().getStatus() + " : " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        //ListView OnClick
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String id_properti = ((TextView) view.findViewById(R.id.properti_id)).getText().toString();
                Intent i = new Intent(getApplicationContext(), DeveloperMenu.class);
                i.putExtra("IDProperti", id_properti);
                startActivity(i);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final Button add_properti = (Button) findViewById(R.id.add_properti);
        add_properti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //popup
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                final View popupView = layoutInflater.inflate(R.layout.properti_add, null);

                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                );
                //popup input
                popupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
                popupWindow.setFocusable(true);
                popupWindow.setOutsideTouchable(false);
                popupWindow.update();

                //cancel button
                Button dismiss = (Button) popupView.findViewById(R.id.btn_cancel);
                dismiss.setOnClickListener(new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });

                //add properti
                Button add = (Button) popupView.findViewById(R.id.btn_add_prop);
                add.setOnClickListener(new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextView tv_nama = (TextView) popupView.findViewById(R.id.et_nama);
                        TextView tv_lokasi = (TextView) popupView.findViewById(R.id.et_lokasi);
                        TextView tv_unit = (TextView) popupView.findViewById(R.id.et_unit);
                        git.addProperti(tv_nama.getText().toString(), tv_lokasi.getText().toString(), tv_unit.getText().toString(), new Callback<Properti>() {

                            @Override
                            public void success(Properti properti, Response response) {
                                Toast.makeText(getApplicationContext(), "Data berhasil dimasukkan", Toast.LENGTH_SHORT).show();
                                Intent in = getIntent();
                                finish();
                                startActivity(in);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(getBaseContext(), error.getResponse().getStatus() + " : " + error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });

                popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

            }
        });
        //popup
        final PopupWindow add_window = new PopupWindow(this);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> av, View v, final int pos, long id) {
                LayoutInflater li = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                final View popupView = li.inflate(R.layout.popup_delete, null);
                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                );
                Button btn_cancel = (Button) popupView.findViewById(R.id.btn_cancel);
                Button btn_ok = (Button) popupView.findViewById(R.id.btn_ok);
                btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        git.deleteProperti(propertiList.get(pos).getIDProperti(), new Callback<Properti>() {
                            @Override
                            public void success(Properti properti, Response response) {
                                Toast.makeText(getBaseContext(), "Properti berhasil di delete!", Toast.LENGTH_LONG);
                                Intent in = getIntent();
                                finish();
                                startActivity(in);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(getBaseContext(), error.getResponse().getStatus() + " : " + error.getMessage(), Toast.LENGTH_LONG);

                            }
                        });
                    }
                });

                popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
                return true;
            }
        });



    }
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {

        // Handle navigation view item clicks here.
        int id = menuItem.getItemId();
        if (id == R.id.nav_logout) {
            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
