package com.example.projectmobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.projectmobile.Certificate.SelfSigningClient;
import com.example.projectmobile.Interface.PropertiInterface;
import com.example.projectmobile.Model.BiayaProyek;
import com.example.projectmobile.Model.NamaBiaya;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class DeveloperMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */

        //Building Retrofit Rest Adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                //set the path
                .setEndpoint(MainActivity.API)
                        //SSL Certificate for HTTPS
                .setClient(new OkClient(SelfSigningClient.createClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        //API Service
        final PropertiInterface git = restAdapter.create(PropertiInterface.class);
        final ListView listView = (ListView) findViewById(R.id.list_properti);
        final Spinner spinner = (Spinner) findViewById(R.id.dev_namabiaya);

        //Get Spinner Value (NamaBiaya)
        git.getNamaBiaya(new Callback<List<NamaBiaya>>() {
            @Override
            public void success(List<NamaBiaya> namaBiayas, Response response) {
                String[] str_namabiaya = new String[namaBiayas.size()];
                for(int i = 0; i < namaBiayas.size(); i++){
                    str_namabiaya[i] = namaBiayas.get(i).getNamaBiaya();
                }
                ArrayAdapter<String> aa_namabiaya = new ArrayAdapter<String>(getBaseContext(), R.layout.support_simple_spinner_dropdown_item, str_namabiaya);
                spinner.setAdapter(aa_namabiaya);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getBaseContext(),error.getResponse().getStatus()+" : "+error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        Button submit = (Button) findViewById(R.id.btn_submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Variables for Biaya Proyek
                EditText et_devdetail = (EditText) findViewById(R.id.dev_detail);
                EditText et_devket = (EditText) findViewById(R.id.dev_keterangan);
                EditText et_devbiaya = (EditText) findViewById(R.id.dev_biaya);
                Intent intent = getIntent();

                //Post Biaya Proyek
                git.postBiayaProyek(
                        intent.getStringExtra("IDProperti"),
                        String.valueOf(spinner.getSelectedItemPosition() + 1),
                        et_devdetail.getText().toString(),
                        et_devket.getText().toString(),
                        et_devbiaya.getText().toString(),
                        new SimpleDateFormat("yyyy-MM-dd").format(new Date()),
                        new Callback<BiayaProyek>() {
                            @Override
                            public void success(BiayaProyek biayaProyek, Response response) {
                                Toast.makeText(getBaseContext(), "Data berhasil dimasukkan", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(getBaseContext(),error.getResponse().getStatus()+" : "+error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }

                );
            }
        });


    }

}
