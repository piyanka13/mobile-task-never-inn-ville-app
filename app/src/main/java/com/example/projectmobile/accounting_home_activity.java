package com.example.projectmobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmobile.Adapter.PropertiAdapter;
import com.example.projectmobile.Certificate.SelfSigningClient;
import com.example.projectmobile.Interface.PropertiInterface;
import com.example.projectmobile.Model.Properti;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class accounting_home_activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accounting_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */

        //Building Retrofit Rest Adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                //set the path
                .setEndpoint(MainActivity.API)
                        //SSL Certificate for HTTPS
                .setClient(new OkClient(SelfSigningClient.createClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        //API Service
        PropertiInterface git = restAdapter.create(PropertiInterface.class);

        final ListView listView = (ListView) findViewById(R.id.list_properti);

        //Calling API Service Function
        git.getFeed(new Callback<List<Properti>>() {

            //If Successful
            @Override
            public void success(List<Properti> users, Response response) {
                listView.setAdapter(new PropertiAdapter(getBaseContext(), R.layout.list_properti, users));
            }

            //If Failed
            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getBaseContext(), error.getResponse().getStatus() + " : " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String i_nama = ((TextView) view.findViewById(R.id.properti_nama)).getText().toString();
                String i_id = ((TextView) view.findViewById(R.id.properti_id)).getText().toString();
                Intent i = new Intent(getApplicationContext(), DetailKeuanganActivity.class);
                i.putExtra("NamaProperti", i_nama);
                i.putExtra("IDProperti", i_id);
                startActivity(i);
            }
        });

        Button btn_pay = (Button) findViewById(R.id.btn_pay);
        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), DetailPembayaranActivity.class);
                startActivity(i);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {

            // Handle navigation view item clicks here.
            int id = menuItem.getItemId();
        if (id == R.id.nav_logout) {
            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
