package com.example.projectmobile;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.Toast;


public class StartActivity  extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.logo_page);

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        final ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", 0, 500); // see this max value coming back here, we animale towards that value
        Thread splashTread = new Thread() {
        @Override
        public void run() {
            try {
                int waited = 10;
                while(waited <= 50) {
                    sleep(100);
                    progressBar.setProgress(waited * 2);
                    waited ++;
                };
            } catch (InterruptedException e) {

            } finally {
                startActivity(new Intent(getBaseContext(), LoginActivity.class));
                finish();
                //stop();
            }
        }
    };
    splashTread.start();
    }

}
