package com.example.projectmobile.Model;


public class BiayaProyek
{
    private String IDNamaBiaya;

    private String IDBiaya;

    private String Tanggal;

    private String IDProperti;

    private String Keterangan;

    private String NamaBiayaDetail;

    private String Biaya;

    public String getIDNamaBiaya ()
    {
        return IDNamaBiaya;
    }

    public void setIDNamaBiaya (String IDNamaBiaya)
    {
        this.IDNamaBiaya = IDNamaBiaya;
    }

    public String getIDBiaya ()
    {
        return IDBiaya;
    }

    public void setIDBiaya (String IDBiaya)
    {
        this.IDBiaya = IDBiaya;
    }

    public String getTanggal ()
    {
        return Tanggal;
    }

    public void setTanggal (String Tanggal)
    {
        this.Tanggal = Tanggal;
    }

    public String getIDProperti ()
    {
        return IDProperti;
    }

    public void setIDProperti (String IDProperti)
    {
        this.IDProperti = IDProperti;
    }

    public String getKeterangan ()
    {
        return Keterangan;
    }

    public void setKeterangan (String Keterangan)
    {
        this.Keterangan = Keterangan;
    }

    public String getNamaBiayaDetail ()
    {
        return NamaBiayaDetail;
    }

    public void setNamaBiayaDetail (String NamaBiayaDetail)
    {
        this.NamaBiayaDetail = NamaBiayaDetail;
    }

    public String getBiaya ()
    {
        return Biaya;
    }

    public void setBiaya (String Biaya)
    {
        this.Biaya = Biaya;
    }

}

