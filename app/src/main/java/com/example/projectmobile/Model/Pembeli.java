package com.example.projectmobile.Model;


public class Pembeli
{
    private String NoKTP;

    private String NamaPembeli;

    private String IDPembeli;

    private String TanggalLahir;

    private String NoHP;

    private String TempatLahir;

    public String getNoKTP ()
    {
        return NoKTP;
    }

    public void setNoKTP (String NoKTP)
    {
        this.NoKTP = NoKTP;
    }

    public String getNamaPembeli ()
    {
        return NamaPembeli;
    }

    public void setNamaPembeli (String NamaPembeli)
    {
        this.NamaPembeli = NamaPembeli;
    }

    public String getIDPembeli ()
    {
        return IDPembeli;
    }

    public void setIDPembeli (String IDPembeli)
    {
        this.IDPembeli = IDPembeli;
    }

    public String getTanggalLahir ()
    {
        return TanggalLahir;
    }

    public void setTanggalLahir (String TanggalLahir)
    {
        this.TanggalLahir = TanggalLahir;
    }

    public String getNoHP ()
    {
        return NoHP;
    }

    public void setNoHP (String NoHP)
    {
        this.NoHP = NoHP;
    }

    public String getTempatLahir ()
    {
        return TempatLahir;
    }

    public void setTempatLahir (String TempatLahir)
    {
        this.TempatLahir = TempatLahir;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [NoKTP = "+NoKTP+", NamaPembeli = "+NamaPembeli+", IDPembeli = "+IDPembeli+", TanggalLahir = "+TanggalLahir+", NoHP = "+NoHP+", TempatLahir = "+TempatLahir+"]";
    }
}