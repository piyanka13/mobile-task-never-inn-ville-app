package com.example.projectmobile.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Properti {

    @SerializedName("IDProperti")
    @Expose
    private String IDProperti;
    @SerializedName("NamaProyek")
    @Expose
    private String NamaProyek;
    @SerializedName("Lokasi")
    @Expose
    private String Lokasi;
    @SerializedName("JumlahUnitTotal")
    @Expose
    private String JumlahUnitTotal;

    /**
     *
     * @return
     * The IDProperti
     */
    public String getIDProperti() {
        return IDProperti;
    }

    /**
     *
     * @param IDProperti
     * The IDProperti
     */
    public void setIDProperti(String IDProperti) {
        this.IDProperti = IDProperti;
    }

    /**
     *
     * @return
     * The NamaProyek
     */
    public String getNamaProyek() {
        return NamaProyek;
    }

    /**
     *
     * @param NamaProyek
     * The NamaProyek
     */
    public void setNamaProyek(String NamaProyek) {
        this.NamaProyek = NamaProyek;
    }

    /**
     *
     * @return
     * The Lokasi
     */
    public String getLokasi() {
        return Lokasi;
    }

    /**
     *
     * @param Lokasi
     * The Lokasi
     */
    public void setLokasi(String Lokasi) {
        this.Lokasi = Lokasi;
    }

    /**
     *
     * @return
     * The JumlahUnitTotal
     */
    public String getJumlahUnitTotal() {
        return JumlahUnitTotal;
    }

    /**
     *
     * @param JumlahUnitTotal
     * The JumlahUnitTotal
     */
    public void setJumlahUnitTotal(String JumlahUnitTotal) {
        this.JumlahUnitTotal = JumlahUnitTotal;
    }

}