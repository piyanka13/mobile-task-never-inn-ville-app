package com.example.projectmobile.Adapter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class User{

    //Variable

    @SerializedName("UserID")
    @Expose
    private String UserID;
    @SerializedName("Username")
    @Expose
    private String Username;
    @SerializedName("Password")
    @Expose
    private String Password;
    @SerializedName("DisplayName")
    @Expose
    private String DisplayName;
    @SerializedName("LastLoggedIn")
    @Expose
    private String LastLoggedIn;

    //Getter and Setter

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String UserID) {
        this.UserID = UserID;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String DisplayName) {
        this.DisplayName = DisplayName;
    }

    public String getLastLoggedIn() {
        return LastLoggedIn;
    }

    public void setLastLoggedIn(String LastLoggedIn) {
        this.LastLoggedIn = LastLoggedIn;
    }

}
