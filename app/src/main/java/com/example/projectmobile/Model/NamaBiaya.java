package com.example.projectmobile.Model;



public class NamaBiaya
{
    private String IDNamaBiaya;

    private String NamaBiaya;

    public String getIDNamaBiaya ()
    {
        return IDNamaBiaya;
    }

    public void setIDNamaBiaya (String IDNamaBiaya)
    {
        this.IDNamaBiaya = IDNamaBiaya;
    }

    public String getNamaBiaya ()
    {
        return NamaBiaya;
    }

    public void setNamaBiaya (String NamaBiaya)
    {
        this.NamaBiaya = NamaBiaya;
    }
}