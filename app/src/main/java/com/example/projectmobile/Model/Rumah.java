package com.example.projectmobile.Model;


public class Rumah {
        private String HargaJualDasar;

        private String IDProperti;

        private String TipeRumah;

        private String UkuranTipe;

        private String IDRumah;

        private String NoRumah;

        public String getHargaJualDasar ()
        {
            return HargaJualDasar;
        }

        public void setHargaJualDasar (String HargaJualDasar)
        {
            this.HargaJualDasar = HargaJualDasar;
        }

        public String getIDProperti ()
        {
            return IDProperti;
        }

        public void setIDProperti (String IDProperti)
        {
            this.IDProperti = IDProperti;
        }

        public String getTipeRumah ()
        {
            return TipeRumah;
        }

        public void setTipeRumah (String TipeRumah)
        {
            this.TipeRumah = TipeRumah;
        }

        public String getUkuranTipe ()
        {
            return UkuranTipe;
        }

        public void setUkuranTipe (String UkuranTipe)
        {
            this.UkuranTipe = UkuranTipe;
        }

        public String getIDRumah ()
        {
            return IDRumah;
        }

        public void setIDRumah (String IDRumah)
        {
            this.IDRumah = IDRumah;
        }

        public String getNoRumah ()
        {
            return NoRumah;
        }

        public void setNoRumah (String NoRumah)
        {
            this.NoRumah = NoRumah;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [HargaJualDasar = "+HargaJualDasar+", IDProperti = "+IDProperti+", TipeRumah = "+TipeRumah+", UkuranTipe = "+UkuranTipe+", IDRumah = "+IDRumah+", NoRumah = "+NoRumah+"]";
        }

}
