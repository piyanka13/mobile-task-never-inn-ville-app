package com.example.projectmobile.Model;


public class Pembelian
{
    private String JenisPembelian;

    private String HargaBeli;

    private String NamaMarketing;

    private String BookingFee;

    private String IDPembeli;

    private String IDPembelian;

    private String IDRumah;

    private String TanggalBooking;

    public String getJenisPembelian ()
    {
        return JenisPembelian;
    }

    public void setJenisPembelian (String JenisPembelian)
    {
        this.JenisPembelian = JenisPembelian;
    }

    public String getHargaBeli ()
    {
        return HargaBeli;
    }

    public void setHargaBeli (String HargaBeli)
    {
        this.HargaBeli = HargaBeli;
    }

    public String getNamaMarketing ()
    {
        return NamaMarketing;
    }

    public void setNamaMarketing (String NamaMarketing)
    {
        this.NamaMarketing = NamaMarketing;
    }

    public String getBookingFee ()
    {
        return BookingFee;
    }

    public void setBookingFee (String BookingFee)
    {
        this.BookingFee = BookingFee;
    }

    public String getIDPembeli ()
    {
        return IDPembeli;
    }

    public void setIDPembeli (String IDPembeli)
    {
        this.IDPembeli = IDPembeli;
    }

    public String getIDPembelian ()
    {
        return IDPembelian;
    }

    public void setIDPembelian (String IDPembelian)
    {
        this.IDPembelian = IDPembelian;
    }

    public String getIDRumah ()
    {
        return IDRumah;
    }

    public void setIDRumah (String IDRumah)
    {
        this.IDRumah = IDRumah;
    }

    public String getTanggalBooking ()
    {
        return TanggalBooking;
    }

    public void setTanggalBooking (String TanggalBooking)
    {
        this.TanggalBooking = TanggalBooking;
    }

    @Override
    public String toString()
    {
        return "JenisPembelian = "+JenisPembelian+", HargaBeli = "+HargaBeli+", NamaMarketing = "+NamaMarketing+", BookingFee = "+BookingFee+", IDPembeli = "+IDPembeli+", IDPembelian = "+IDPembelian+", IDRumah = "+IDRumah+", TanggalBooking = "+TanggalBooking;
    }
}