package com.example.projectmobile.Model;


public class PembelianDetail
{
    private String TanggalBayar;

    private String IDDetailPembelian;

    private String Jumlah;

    private String Keterangan;

    private String IDPembelian;

    public String getTanggalBayar ()
    {
        return TanggalBayar;
    }

    public void setTanggalBayar (String TanggalBayar)
    {
        this.TanggalBayar = TanggalBayar;
    }

    public String getIDDetailPembelian ()
    {
        return IDDetailPembelian;
    }

    public void setIDDetailPembelian (String IDDetailPembelian)
    {
        this.IDDetailPembelian = IDDetailPembelian;
    }

    public String getJumlah ()
    {
        return Jumlah;
    }

    public void setJumlah (String Jumlah)
    {
        this.Jumlah = Jumlah;
    }

    public String getKeterangan ()
    {
        return Keterangan;
    }

    public void setKeterangan (String Keterangan)
    {
        this.Keterangan = Keterangan;
    }

    public String getIDPembelian ()
    {
        return IDPembelian;
    }

    public void setIDPembelian (String IDPembelian)
    {
        this.IDPembelian = IDPembelian;
    }

}